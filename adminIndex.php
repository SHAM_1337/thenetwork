<?php

require_once 'vendor/autoload.php';

require_once 'init.php';

require_once 'utils.php';

require_once 'index.php';

// Define app routes
$app->get('/admin', function ($request, $response, $args) {
    $userList = DB::query("SELECT * FROM users ORDER BY userId ASC");
    if (!isset($_SESSION['currentUser'])) {
        return $this->view->render($response, 'admin/adminIndex.html.twig');
    } else {
        return $this->view->render($response, 'admin/adminIndex.html.twig', ['currentUser' => $_SESSION['currentUser']]);
    }
});

$app->get('/admin/profile/{userId:[0-9]+}', function ($request, $response, $args) {
    $selectedUser = DB::queryFirstRow("SELECT * FROM users WHERE userId=%d", $args['userId']);
    return $this->view->render($response, 'admin/adminProfile.html.twig', ['selectedUser' => $selectedUser]);
});

$app->get('/admin/manage', function ($request, $response, $args) {
    $userList = DB::query("SELECT * FROM users ORDER BY userId ASC");
    return $this->view->render($response, 'admin/adminUserManage.html.twig', ['userList' => $userList]);
});

$app->get('/admin/reports', function ($request, $response, $args) {
    $reportList = DB::query("SELECT * FROM users WHERE isReported='1' ORDER BY userId ASC");
    //$reportList = DB::query("SELECT * FROM users ORDER BY userId ASC");
    return $this->view->render($response, 'admin/adminReports.html.twig', ['reportList' => $reportList]);
});

$app->get('/admin/reports/user/{userId:[0-9]+}', function ($request, $response, $args) {
    $badUser = DB::queryFirstRow("SELECT * FROM users WHERE userId=%d", $args['userId']);
    $reports = DB::query(
        "SELECT 
        r.reportId, r.type, r.plaintiffId, r.note, r.createdAt, r.postId, r.commentId, u.firstName
        FROM 
        reports as r 
        INNER JOIN 
        users as u 
        ON 
        r.plaintiffId = u.userId
        AND
        r.userId=%d",
        $args['userId']
    );
    if ($badUser) {
        return $this->view->render($response, 'admin/adminReportsUser.html.twig', ['badUser' => $badUser, 'reports' => $reports]);
    } else { // not found - cause 404 here
        throw new \Slim\Exception\NotFoundException($request, $response);
    }
});
// function calculateLikes($id) {
//     $total = 0;

//     while ($num = DB::query("SELECT likes FROM comments WHERE userId=$id")) {
//         $total += $num['likes'];
//     }

//     while ($num = DB::query("SELECT likes FROM posts WHERE userId=$id")) {
//         $total += $num['likes'];
//     }

//     return $total;
// }
// get User
$app->get('/admin/manage/user/{userId:[0-9]+}', function ($request, $response, $args) use ($log) {
    $selectedUser = DB::queryFirstRow("SELECT * FROM users WHERE userId=%d", $args['userId']);
    // $likes = DB::query(
    //     "SELECT 
    //     p.likes, c.likes
    //     FROM 
    //     posts as p 
    //     INNER JOIN 
    //     comments as c 
    //     ON 
    //     p.userId = c.userId
    //     AND
    //     p.userId=%d",
    //     $args['userId']
    // );
    // $likes = calculateLikes($args['userId']);
    if ($selectedUser) {
        return $this->view->render($response, 'admin/adminUserManageSelected.html.twig', [
            'selectedUser' => $selectedUser //, 'likes' => $likes
        ]);
    } else { // not found - cause 404 here
        throw new \Slim\Exception\NotFoundException($request, $response);
    }
});

// Update user
// tried with REST API initially, couldn't figure it out
$app->post('/admin/manage/user/{userId:[0-9]+}', function ($request, $response, $args) use ($log) {
    $selectedUser = DB::queryFirstRow("SELECT * FROM users WHERE userId=%d", $args['userId']);
    $location = $_SERVER['REQUEST_URI'];
    $FN = $request->getParam('FN');
    $MN = $request->getParam('MN');
    $LN = $request->getParam('LN');
    $date = $request->getParam('dob');
    $emailcurrent = $selectedUser['emailAddress'];
    $emailnew = $request->getParam('email');
    $city = $request->getParam('city');
    $password = $request->getParam('pass');
    $active = $request->getParam('accountActive');
    if ($active == "Yes") {
        $active = 1;
    }
    if ($active == "No") {
        $active = 0;
    }
    $reported = $request->getParam('accountReported');
    if ($reported == "Yes") {
        $reported = 1;
    }
    if ($reported == "No") {
        $reported = 0;
    }
    $banned = $request->getParam('accountBanned');
    if ($banned == "Yes") {
        $banned = 1;
    }
    if ($banned == "No") {
        $banned = 0;
    }
    $created = $request->getParam('created');
    $edited = Date('Y-m-d H:i:s');

    $errorList = [];
    if (preg_match('/^[a-zA-Z0-9 \,\'\.\-]{2,100}$/', $FN) !== 1) {
        $errorList[] = "First name must be 2-100 characters long, accepted characters are uppercase/lowercase letters, spaces, commas, hypens and apostrophes.";
    }
    if ($MN != null) {
        if (preg_match('/^[a-zA-Z0-9 \,\'\.\-]{2,100}$/', $MN) !== 1) {
            $errorList[] = "Middle name must be 2-100 characters long, accepted characters are uppercase/lowercase letters, spaces, commas, hypens and apostrophes.";
        }
    }
    if (preg_match('/^[a-zA-Z0-9 \,\'\.\-]{2,100}$/', $LN) !== 1) {
        $errorList[] = "Last name must be 2-100 characters long, accepted characters are uppercase/lowercase letters, spaces, commas, hypens and apostrophes.";
    }
    if (filter_var($emailnew, FILTER_VALIDATE_EMAIL) === FALSE) {
        $errorList[] = "Email does not look valid";
    } else {
        $result = DB::queryFirstRow("SELECT * FROM users WHERE emailAddress='$emailnew'");
        if (!$result) {
            echo "SQL Query failed";
            exit;
        }
        if ($result && $emailnew != $emailcurrent) {
            $errorList[] = "This email is already registered!";
        }
    }
    if (validateDate($date)) {
        $DoB = DateTime::createFromFormat('d/m/Y', $date);
    } else {
        $errorList[] = "Please enter a valid date in the format dd/mm/YYYY";
    }
    if ($city != null) {
        if (preg_match('/^[a-zA-Z0-9 \,\'\.\-]{2,100}$/', $city) !== 1) {
            $errorList[] = "City name must be 1-150 characters long, accepted characters are uppercase/lowercase letters, spaces, commas, hypens and apostrophes.";
        }
    }
    // verify image
    $hasPhoto = false;
    $mimeType = "";
    $uploadedImage = $request->getUploadedFiles()['image'];
    if ($uploadedImage->getError() != UPLOAD_ERR_NO_FILE) { // was anything uploaded?
        // print_r($uploadedImage->getError());
        $hasPhoto = true;
        $result = verifyUploadedPhoto($uploadedImage, $mimeType);
        if ($result !== TRUE) {
            $errorList[] = $result;
        }
    }
    if ($errorList) { // STATE 2: errors
        setFlashMessage("Errors");
        return $this->view->render($response, 'admin/adminUserManageSelected.html.twig', ['selectedUser' => $selectedUser, 'errorList' => $errorList]);
    } else { // STATE 3: success
        $photoData = null;
        $uploadedImagePath = null;
        if ($hasPhoto) {
            $photoData = file_get_contents($uploadedImage->file);
            $directory = $this->get('upload_directory');
            $uploadedImagePath = moveUploadedFile($directory, $uploadedImage);
        }
        // overwrite with new picture
        if ($uploadedImagePath) {
            // this stopped working ERROR main.ERROR: Database error: Badly formatted SQL query: Expected array, got scalar instead!
            // $valuesList = [
            //     'firstName' => $FN,
            //     'lastName' => $LN,
            //     'middleName' => $MN,
            //     'emailAddress' => $emailnew,
            //     'dateofBirth' => $DoB,
            //     'currentCity' => $city,
            //     'profilePicturePath' => $uploadedImagePath,
            //     'updatedAt' => $edited
            // ];
            // either update password, or leave
            if ($password != "") { // this is super messy, but we couldn't figure out the error
                DB::update('users',[
                    'firstName' => $FN,
                    'lastName' => $LN,
                    'middleName' => $MN,
                    'emailAddress' => $emailnew,
                    'dateofBirth' => $DoB,
                    'currentCity' => $city,
                    'profilePicturePath' => $uploadedImagePath,
                    'password' => $password,
                    'updatedAt' => $edited
                ], "userId=%i", $args['userId']);
            } else {
                DB::update('users',[
                    'firstName' => $FN,
                    'lastName' => $LN,
                    'middleName' => $MN,
                    'emailAddress' => $emailnew,
                    'dateofBirth' => $DoB,
                    'currentCity' => $city,
                    'profilePicturePath' => $uploadedImagePath,
                    'updatedAt' => $edited
                ], "userId=%i", $args['userId']);
            }
        }
        // keep old picture
        else if ($selectedUser['profilePicturePath'] && $uploadedImagePath == null) {
            // $valuesList = [
            //     'firstName' => $FN,
            //     'lastName' => $LN,
            //     'middleName' => $MN,
            //     'emailAddress' => $emailnew,
            //     'dateofBirth' => $DoB,
            //     'currentCity' => $city,
            //     'profilePicturePath' => $selectedUser['profilePicturePath'],
            //     'updatedAt' => $edited
            // ];
            // either update password, or leave
            if ($password != "") { // this is super messy, but we couldn't figure out the error
                DB::update('users',[
                    'firstName' => $FN,
                    'lastName' => $LN,
                    'middleName' => $MN,
                    'emailAddress' => $emailnew,
                    'dateofBirth' => $DoB,
                    'currentCity' => $city,
                    'profilePicturePath' => $selectedUser['profilePicturePath'],
                    'password' => $password,
                    'updatedAt' => $edited
                ], "userId=%i", $args['userId']);
            } else {
                DB::update('users',[
                    'firstName' => $FN,
                    'lastName' => $LN,
                    'middleName' => $MN,
                    'emailAddress' => $emailnew,
                    'dateofBirth' => $DoB,
                    'currentCity' => $city,
                    'profilePicturePath' => $selectedUser['profilePicturePath'],
                    'updatedAt' => $edited
                ], "userId=%i", $args['userId']);
            }
        } else { // no picture on account, no picture to upload
            // either update password, or leave
            if ($password != "") { // this is super messy, but we couldn't figure out the error
                DB::update('users',[
                    'firstName' => $FN,
                    'lastName' => $LN,
                    'middleName' => $MN,
                    'emailAddress' => $emailnew,
                    'dateofBirth' => $DoB,
                    'currentCity' => $city,
                    'password' => $password,
                    'updatedAt' => $edited
                ], "userId=%i", $args['userId']);
            } else {
                DB::update('users',[
                    'firstName' => $FN,
                    'lastName' => $LN,
                    'middleName' => $MN,
                    'emailAddress' => $emailnew,
                    'dateofBirth' => $DoB,
                    'currentCity' => $city,
                    'updatedAt' => $edited
                ], "userId=%i", $args['userId']);
            } 
        }
        // DB::update('users', $valuesList, "userId=%i", $args['userId']);
        // $log->debug("User with userId=%s updated from IP=%s", $args['userId'], $_SERVER['REMOTE_ADDR']);
        setFlashMessage("User Account Successfully Updated!");
        return $response->withStatus(302)->withHeader(
            'Location',
            $location
            //'http://localhost:4433/ipd23/FebProjectMine/Adminindex.php/adminUserManage'
            // 'http://localhost:4433/ipd23/FebProjectMine/Adminindex.php/adminUserManageSelected/' . $userId
        );
    }
});

// $app->map(['PUT', 'PATCH'], '/adminUserManageSelected/{id:[0-9]+}', function ($request, $response, array $args) use ($log) {
//     $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
//     $json = $request->getBody();
//     $user = json_decode($json, TRUE); // true makes it return an associative array instead of an object
//     // TODO: validate
//     $initialUser = DB::queryFirstRow("SELECT * FROM users WHERE userId=%i", $args['userId']);
//     if (!$initialUser) { // record not found
//         $response = $response->withStatus(404);
//         $response->getBody()->write(json_encode("404 - not found"));
//         return $response;
//     }
//     DB::update('users', $user, "userId=%i", $args['userId']);
//     $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses
//     return $response;
// });
