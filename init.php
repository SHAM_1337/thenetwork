<?php

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Slim\Views\Twig;
use Slim\Http\UploadedFile;

$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

$log->pushProcessor(function ($record) {
    $record['extra']['currentUser'] = isset($_SESSION['currentUser']) ? $_SESSION['currentUser']['emailAddress'] : '=anonymous';
    $record['extra']['ip'] = $_SERVER['REMOTE_ADDR'];
    return $record;
});


// Database setup
if (strpos($_SERVER['HTTP_HOST'], "ipd23.com") !== false) {
    // Hosting on ipd23.com database connection setup
    DB::$dbName = 'cp4996_thenetwork';
    DB::$user = 'cp4996_thenetwork';
    DB::$password = 'qSsjC2mO55jUtK2I';
} else {
    // Local host setup
    DB::$dbName = 'thenetwork';
    DB::$user = 'thenetwork';
    DB::$password = 'qSsjC2mO55jUtK2I';
    DB::$host = 'localhost';
    DB::$port = 3333;
}

DB::$error_handler = 'db_error_handler';
DB::$nonsql_error_handler = 'db_error_handler';

function db_error_handler($params)
{
    global $log, $container;

    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL Query: " . $params['query']);
    }

    http_response_code(500);
    header('Content-type: json/application; charset=UTF-8');
    die(json_encode("500 - Internal Error"));
}

// Create and configure Slim app
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

//Override the default Not Found Handler before creating App
$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - not found"));
        return $response;
    };
};

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/tmplcache',
        'debug' => true, // This line should enable debug mode
    ]);
    //
    $view->getEnvironment()->addGlobal('test1', 'VALUE');
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};

// File upload directory
$container['upload_directory'] = __DIR__ . '/uploads';
