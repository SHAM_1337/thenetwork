<?php 

require_once 'init.php';

// tutorial here
// https://phppot.com/php/creating-dynamic-data-graph-using-php-and-chart-js/

$userList = DB::query("SELECT users.userId, users.isReported FROM users ORDER BY userId ASC");

// $data = array();
// foreach ($userList as $row) {
// 	$data[] = $row;
// }

header('Content-type: application/json');

echo json_encode($userList);
